# Gothamik

## What is it?

Gothamik is a tool to easily create titles and texts based on the Gotham font family, with respect to the visual identity of the CNSMDP. It specifically adhere to the concept of "typographical rhythms" which requires the use of different font weights for each separate character.

## Status

Currently in development, to be publicly released sometime in the future.

## Known issues

- SVG export of large texts can be truncated with Safari. Please use Firefox or a Chromium-based browser.