const project = {
    name: "Gothamik",
    version: "0.1.0",
    params: {
        svgLayerNameMaxLength: 80
    }
};

const phrase = document.getElementById ('phrase');
const fontSize = document.getElementById ('font-size');
const fontColor = document.getElementById ('font-color');
const canvas = document.getElementById ('canvas');
const ctx = canvas.getContext('2d');
const svgPath = document.getElementById('svgPath');


let fonts = [];

async function addFont (name, src)
{
    await opentype.load (src).then (function (type) {
        fonts.push({ name, src, type: type });
    });
}

async function loadFonts() {
    await addFont ('Light',  'fonts/Gotham-Light.otf');
    await addFont ('Book',   'fonts/Gotham-Book.otf');
    await addFont ('Medium', 'fonts/Gotham-Medium.otf');
    await addFont ('Bold',   'fonts/Gotham-Bold.otf');
    await addFont ('Black',  'fonts/Gotham-Black.otf');
    await addFont ('Ultra',  'fonts/Gotham-Ultra.otf');
};


loadFonts().then (() => {
    phrase.value = "#CONSERVATOIRE\n*NATIONAL *SUPÉRIEUR\nDE *MUSIQUE ET\nDE *DANSE #DE PARIS";
    updateCanvas();
}, () => console.error("Woops..."));


window.addEventListener ('resize', resizeCanvas, false);
document.getElementById ('download-svg').addEventListener ('click', trigDownloadSVG);
document.getElementById ('download-png').addEventListener ('click', trigDownloadPNG);

phrase.oninput = function () {
    updateCanvas ();
};

fontSize.oninput = function () {
    updateCanvas();
}

fontColor.oninput = function () {
    updateCanvas();
}

function resizeCanvas()
{
    updateCanvas ();
}

function resizeCanvasToDisplaySize () {
    // look up the size the canvas is being displayed
    const width = canvas.clientWidth;
    //const height = canvas.clientHeight;
    const height = parseInt (fontSize.value) * (phrase.value.split("\n").length + 0.25);
    
    // If it's resolution does not match change it
    if (canvas.width !== width || canvas.height !== height) {
        canvas.width = width;
        canvas.height = height;
        return true;
    }
    
    return false;
}

function render (phrase, originX, originY, size, color)
{
    const maxWeight = 5;
    
    let x = originX;
    let y = originY + size;
    let weight = 0;
    let rate = 0.0;
    let previousChar = "";
    let previousCharIndex = -1;
    let renderPath = new opentype.Path();

    for (let i = 0; i < phrase.length; ++i)
    {
        let char = phrase[i];

        if (char == "#")
        {
            weight = maxWeight;
            rate = 0.0;
        }
        else if (char == "*")
        {
            weight = maxWeight;
            rate = 1.0;
        }
        else if (char == "\n")
        {
            x = originX;
            y += size;
        }
        else if (char == ">")
        {
            for (let s = i + 1; s < phrase.length; ++s)
            {
                rate = maxWeight / (s - i);
                if (phrase[s] == "!" || phrase[s] == "<")
                {
                    weight = maxWeight;
                    break;
                }
            }
        }
        else if (char == "<")
        {
            for (let s = i + 1; s < phrase.length; ++s)
            {
                rate = - (maxWeight / (s - i));
                if (phrase[s] == "!" || phrase[s] == ">")
                {
                    weight = 0;
                    break;
                }
            }
        }
        else if (char == "!")
        {
            weight = 0.
            rate = 0.0;
        }
        else
        {
            if (char == "\\")
            {
                if (i < phrase.length - 1)
                {
                    ++i;
                    char = phrase[i];
                }
            }

            const font = fonts[Math.round (weight)].type;
            const scale = size / font.unitsPerEm;
            const glyph = font.charToGlyph (char);
            const kerning = font.getKerningValue (previousCharIndex, glyph.index);
            x += kerning * scale;
            const path = glyph.getPath (x, y, size);
            x += (glyph.advanceWidth) * scale;
            renderPath.extend (path);
            
            weight -= rate;
            if (weight < 0.0)
            {
                weight = 0.0;
            }
            else if (weight > maxWeight)
            {
                weight = maxWeight;
            }
            
            previousChar = char;
            previousCharIndex = glyph.index;
        }
    }

    renderPath.fill = color;
    return renderPath;
}

function updateCanvas()
{
    resizeCanvasToDisplaySize ()

    ctx.clearRect (0, 0, canvas.width, canvas.height);

    const size = parseInt (fontSize.value);
    const path = render (phrase.value, 0, 0, size, fontColor.value);
    path.draw (ctx);
    
    //svgPath.innerText = generateSVG (path);

    // var image = document.createElement('img');
    // image.src = 'data:image/svg+xml,' + generateSVG (path);
    // document.getElementById ("drag").appendChild (image);
}

function trigDownloadSVG()
{
    const size = parseInt (fontSize.value);
    downloadSVG (phrase.value, size, fontColor.value); // TODO: dynamic size
}

function generateSVG (phrase, size, color)
{
    const path = render (phrase, 0, 0, size, color);
    const boundingBox = path.getBoundingBox();
    const viewBox = [boundingBox.x1, boundingBox.y1, boundingBox.x2, boundingBox.y2];
    const layer = phraseToString (phrase).substring (0, project.params.svgLayerNameMaxLength);
    svg  = '<?xml version="1.0" encoding="utf-8"?>';
    svg += '<!-- Generator: ' + project.name + ' (' + project.version + ') -->';
    svg += '<svg version="1.1" id="' + layer + '" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="' + viewBox.join (" ") + '" xml:space="preserve">';
    svg += '  ' + path.toSVG();
    svg += '</svg>';

    return svg;
}

function trigDownloadPNG()
{
    downloadPNG (phrase.value)
}

function downloadPNG (phrase)
{
    canvas.toBlob ((blob) => {
        const url = URL.createObjectURL (blob);
        const filename = phraseToFilename (phrase);
        const a = document.createElement ('a');
    
        a.setAttribute ('href', url);
        a.setAttribute ('download', filename);
        a.click();
    });
}

function downloadSVG (phrase, size, color)
{
    const content = generateSVG (phrase, size, color);
    const filename = phraseToFilename (phrase);
    const a = document.createElement ('a');
    const blob = new Blob ([content], {type: "image/svg+xml"});
    const url = URL.createObjectURL (blob);

    a.setAttribute ('href', url);
    a.setAttribute ('download', filename);
    a.click();
}

function phraseToString (phrase)
{
    let str = "";
    for (let i = 0; i < phrase.length; ++i)
    {
        let char = phrase[i];

        if (char == "#" || char == "*" || char == ">" || char == "<" || char == "!")
        {
        }
        else if (char == "\n")
        {
            str += " ";
        }
        else
        {
            if (char == "\\")
            {
                if (i < phrase.length - 1)
                {
                    ++i;
                    char = phrase[i];
                }
            }
            str += char;
        }
    }
    
    return str;
}

function phraseToFilename (phrase)
{
    return phraseToString (phrase).replaceAll (' ', '_');
}